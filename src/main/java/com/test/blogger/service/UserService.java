package com.test.blogger.service;

import com.test.blogger.modal.User;

public interface UserService {
	
	public void updateUserRoles(String role, String uuid);
	
	public User getUserByID(String uuid);

}
