package com.test.blogger.service;

public interface WorkflowService {
	
	public boolean approveBlog(String userId, String blogId);
	
	public boolean rejectBlog(String userId, String blogId);
	
	public boolean reviewBlog(String userId, String blogId);

}
