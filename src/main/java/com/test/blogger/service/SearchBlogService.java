package com.test.blogger.service;

import java.util.List;

import com.test.blogger.modal.BlogDetails;

public interface SearchBlogService {
	
   public List<BlogDetails> getAllPublishedBlog();
   
   public List<BlogDetails> getAllUnPublishedBlog();
}
