package com.test.blogger.service;

import com.test.blogger.modal.BlogDetails;

public interface BlogService {

	public void createBlog(BlogDetails blogDetails);
	
	public BlogDetails archiveBlog(String blogId);
	
	public void updateBlog(BlogDetails blogDetails);
	
	public BlogDetails getBlog(String blogId); 
	
}
