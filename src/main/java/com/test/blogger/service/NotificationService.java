package com.test.blogger.service;

import java.util.List;

import com.test.blogger.modal.BlogDetails;

public interface NotificationService {
	
	public boolean SendEmail(String to, String from, String subject, String body);

}
