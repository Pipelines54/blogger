package com.test.blogger.modal;

public class Mail {
	
	private String subject;
	
	private String body;
	
	private String to;
	
	private String from;
	
	public Mail(String to, String from, String subject, String body) {
		this.to = to;
		this.from = from;
		this.subject = subject;
		this.body = body;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	@Override
	public String toString() {
		return "Mail [subject=" + subject + ", body=" + body + ", to=" + to + ", from=" + from + "]";
	}
	

}
