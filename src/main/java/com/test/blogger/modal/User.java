package com.test.blogger.modal;

import com.test.blogger.enums.UserRole;

public class User {
	
	private String uuid;
	
	private String name;
	
	private int pinCode;
	
	private UserRole userRole;

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPinCode() {
		return pinCode;
	}

	public void setPinCode(int pinCode) {
		this.pinCode = pinCode;
	}

	public UserRole getUserRole() {
		return userRole;
	}

	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}

	@Override
	public String toString() {
		return "User [uuid=" + uuid + ", name=" + name + ", pinCode=" + pinCode + ", userRole=" + userRole + "]";
	}
	
	

}
