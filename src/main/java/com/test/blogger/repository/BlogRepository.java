package com.test.blogger.repository;

import java.util.Map;

import com.test.blogger.modal.BlogDetails;

public interface BlogRepository {
	
     public void createBlog(BlogDetails blogDetails);
     
 	 public void updateBlog(BlogDetails blogDetails);
 	 
 	 public BlogDetails getBlog(String blogId);
 	 
 	 public Map<String,BlogDetails> getAllBlogs();
}

