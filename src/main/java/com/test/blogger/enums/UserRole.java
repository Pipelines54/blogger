package com.test.blogger.enums;

public enum UserRole {
	
	GUEST,
	AUTHOR,
	REVIEWER,
	PUBLISHER,
	ADMIN
	
}
