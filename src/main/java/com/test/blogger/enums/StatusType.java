package com.test.blogger.enums;

public enum StatusType {
	
	APPROVAL_PENDING,
	FINAL_APPROVAL,
	CONTAIN_REVIEW_PENDING,
	ARCHIVE,
	PUBLISH,
	REJECTED,
	REVIEW,
	PROGRESS
	
}
