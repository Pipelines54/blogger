package com.test.blogger.serviceImpl;

import com.test.blogger.enums.UserRole;
import com.test.blogger.modal.User;
import com.test.blogger.service.UserService;

public class UserServiceImpl implements UserService {

	/**
	 * updateUser Role
	 * @param role
	 * @param userId
	 */
	@Override
	public void updateUserRoles(String role, String userId) {
		
		User user = getUserByID(userId);
		UserRole userRole = UserRole.valueOf(role);
		user.setUserRole(userRole);
		//Skiping user repo... here
		//userRepository.updateUser(user);
	}
	
	/**
	 * Get User By UserId
	 * @param uuid
	 * @return
	 */
	@Override
	public User getUserByID(String uuid) {
		User user = null;
		
		//Skiping user repo... here
		// user = userRepository.getUserByID(uuid); 
		
		return user;
		
	}

}
