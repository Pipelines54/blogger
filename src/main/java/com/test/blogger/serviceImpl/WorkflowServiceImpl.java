package com.test.blogger.serviceImpl;

import com.test.blogger.constants.AppConstants;
import com.test.blogger.enums.StatusType;
import com.test.blogger.enums.UserRole;
import com.test.blogger.modal.BlogDetails;
import com.test.blogger.modal.User;
import com.test.blogger.service.BlogService;
import com.test.blogger.service.NotificationService;
import com.test.blogger.service.UserService;
import com.test.blogger.service.WorkflowService;

public class WorkflowServiceImpl implements WorkflowService {

	private NotificationService notificationService = new NotificationServiceImpl();

	/**
	 * Approve Blog to publish, Only publisher can approve the blog
	 * 
	 * @param userId
	 * @param blogId
	 * @return
	 */
	@Override
	public boolean approveBlog(String userId, String blogId) {
		UserService userService = new UserServiceImpl();
		User user = userService.getUserByID(userId);
		try {
			if (UserRole.PUBLISHER.equals(user.getUserRole())) {
				BlogService blogService = new BlogServiceImpl();
				BlogDetails blogDetails = blogService.getBlog(blogId);
				blogDetails.setStatus(StatusType.FINAL_APPROVAL);
				blogService.updateBlog(blogDetails);
				notificationService.SendEmail(AppConstants.TO_EMAIL, AppConstants.FROM_EMAIL, AppConstants.SUBJECT_APPROVE_EMAIL, "Something");
				return true;
			}
		} catch (Exception e) {
			System.out.println("User is not Authorise for this operation");
		}

		return false;
	}

	/**
	 * Reject Blog to publish, Only publisher can reject the blog
	 * 
	 * @param userId
	 * @param blogId
	 * @return
	 */
	@Override
	public boolean rejectBlog(String userId, String blogId) {
		String blogUpdateId;
		UserService userService = new UserServiceImpl();
		User user = userService.getUserByID(userId);
		try {
			if (UserRole.PUBLISHER.equals(user.getUserRole())) {
				BlogService blogService = new BlogServiceImpl();
				BlogDetails blogDetails = blogService.getBlog(blogId);
				blogDetails.setStatus(StatusType.REJECTED);
				blogService.updateBlog(blogDetails);
				notificationService.SendEmail(AppConstants.TO_EMAIL, AppConstants.FROM_EMAIL, AppConstants.SUBJECT_REJECT_EMAIL, "Something");
				return true;
			}	
		} catch (Exception e) {
			System.out.println("User is not Authorise for this operation");
		}
		return false;
	}

	/**
	 * Review Blog to publish, Only admin can mark the blog as review
	 * 
	 * @param userId
	 * @param blogId
	 * @return
	 */
	@Override
	public boolean reviewBlog(String userId, String blogId) {
		String blogUpdateId;
		UserService userService = new UserServiceImpl();
		User user = userService.getUserByID(userId);
		try {
			if (UserRole.ADMIN.equals(user.getUserRole())) {
				BlogService blogService = new BlogServiceImpl();
				BlogDetails blogDetails = blogService.getBlog(blogId);
				blogDetails.setStatus(StatusType.REVIEW);
				blogService.updateBlog(blogDetails);
				return true;
			}
		} catch (Exception e) {
			System.out.println("User is not Authorise for this operation");
		}
		return false;
	}

}
