package com.test.blogger.serviceImpl;

import java.util.Scanner;

import com.test.blogger.constants.AppConstants;
import com.test.blogger.enums.StatusType;
import com.test.blogger.exception.BlogException;
import com.test.blogger.modal.BlogDetails;
import com.test.blogger.repository.BlogRepository;
import com.test.blogger.repositoryImpl.BlogRepositoryImpl;
import com.test.blogger.service.BlogService;
import com.test.blogger.service.NotificationService;

public class BlogServiceImpl implements BlogService {

	private static Scanner scanner = new Scanner(System.in);
	private NotificationService notificationService = new NotificationServiceImpl();
	private BlogRepository blogRepository = new BlogRepositoryImpl();

	/**
	 * Create new blog Note: As of now UserId will be {{abc5012}} for this example
	 * only
	 * 
	 * @param blogDetails
	 */
	public void createBlog(BlogDetails blogDetails) {
		try {
			// In real senario user not need to enter any uuid by them selves.
			blogDetails.setUserId(AppConstants.USERID);
			System.out.println(AppConstants.BLOG_TITLE_QUESTIONS);
			blogDetails.setBlogTitle(scanner.nextLine());
			blogDetails.setStatus(StatusType.CONTAIN_REVIEW_PENDING);
			blogDetails.setVote(0);

			System.out.println(AppConstants.BLOG_CONTENT_QUESTIONS);
			blogDetails.setBlogContent(scanner.nextLine());

			blogRepository.createBlog(blogDetails);

			notificationService.SendEmail(AppConstants.TO_EMAIL, AppConstants.FROM_EMAIL,
					AppConstants.SUBJECT_CREATE_EMAIL, "Somthing");
		} catch (Exception e) {
			throw new BlogException();
		}

	}

	/**
	 * Archive Blog by BlogId
	 * @param blogId
	 */
	public BlogDetails archiveBlog(String blogId) {
		String userId = AppConstants.USERID;
		BlogDetails blogDetails = blogRepository.getBlog(blogId);
		if (blogDetails != null && blogDetails.getUserId().equals(userId)) {
			blogDetails.setStatus(StatusType.ARCHIVE);
			blogRepository.updateBlog(blogDetails);
			notificationService.SendEmail(AppConstants.TO_EMAIL, AppConstants.FROM_EMAIL,
					AppConstants.SUBJECT_ARCHIVE_EMAIL, "Somthing");
		} else {
			System.out.println(AppConstants.NOT_AUTHORIZED);
		}
		return blogDetails;
	}

	/**
	 * Update Blog by blogId
	 *  Note: As of now only blog title can be editble
	 *  @param blogDetails
	 */
	public void updateBlog(BlogDetails blogDetails) {
		// As of now we are updating only blog title.
		System.out.println(AppConstants.BLOG_TITLE_QUESTIONS);
		blogDetails.setBlogTitle(scanner.nextLine());
		blogRepository.updateBlog(blogDetails);
		notificationService.SendEmail(AppConstants.TO_EMAIL, AppConstants.FROM_EMAIL, AppConstants.SUBJECT_UPDATE_EMAIL,
				"Somthing");
	}

	public BlogDetails getBlog(String blogId) {
		return blogRepository.getBlog(blogId);
	}

}
