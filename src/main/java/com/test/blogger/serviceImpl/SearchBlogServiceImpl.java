package com.test.blogger.serviceImpl;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.test.blogger.enums.StatusType;
import com.test.blogger.modal.BlogDetails;
import com.test.blogger.repository.BlogRepository;
import com.test.blogger.repositoryImpl.BlogRepositoryImpl;
import com.test.blogger.service.SearchBlogService;

public class SearchBlogServiceImpl implements SearchBlogService{

	BlogRepository blogRepository = new BlogRepositoryImpl();
	
	/**
	 * Get All published Blogs
	 */
	@Override
	public List<BlogDetails> getAllPublishedBlog() {
		Map<String,BlogDetails> blogData = (Map<String, BlogDetails>) blogRepository.getAllBlogs();
		List filterPublishedBlog = blogData.values().stream().
												filter(blog -> blog.getStatus() == StatusType.PUBLISH).
																collect(Collectors.toList());
		return filterPublishedBlog;
	}

	/**
	 * Get All unpublished Blogs
	 */
	@Override
	public List<BlogDetails> getAllUnPublishedBlog() {
		Map<String,BlogDetails> blogData = (Map<String, BlogDetails>) blogRepository.getAllBlogs();
		List filterUnPublishedBlog = blogData.values().stream().
												filter(blog -> blog.getStatus() != StatusType.PUBLISH).
																collect(Collectors.toList());
		return filterUnPublishedBlog;
	}

}
