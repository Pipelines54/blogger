package com.test.blogger.serviceImpl;

import com.test.blogger.modal.Mail;
import com.test.blogger.service.NotificationService;

public class NotificationServiceImpl implements NotificationService {

	@Override
	public boolean SendEmail(String to, String from, String subject, String body) {
		boolean isMailSend = false;
		Mail mail = new Mail(to, from, subject, body);
		try {
//			mailSender.sendMail(mail);
			isMailSend = true;
		} catch (Exception e) {
			System.out.println("Mail not getting send to : " + to);
			// TODO: handle exception
		}
		return isMailSend;
	}

}
