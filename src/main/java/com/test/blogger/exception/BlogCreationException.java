package com.test.blogger.exception;

public class BlogCreationException extends RuntimeException{
	
	private static final long serialVersionUID = 1L;

	public BlogCreationException(String message) {
		super(message);
	}
	
	

}
