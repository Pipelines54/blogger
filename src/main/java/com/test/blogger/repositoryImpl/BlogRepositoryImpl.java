package com.test.blogger.repositoryImpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.test.blogger.constants.AppConstants;
import com.test.blogger.enums.StatusType;
import com.test.blogger.exception.BlogCreationException;
import com.test.blogger.exception.BlogException;
import com.test.blogger.modal.BlogDetails;
import com.test.blogger.repository.BlogRepository;
import com.test.blogger.service.NotificationService;
import com.test.blogger.serviceImpl.NotificationServiceImpl;

public class BlogRepositoryImpl implements BlogRepository {
	
	private static Map<String,BlogDetails> blogDetailsData = new HashMap();
	
	private NotificationService notificationService = new NotificationServiceImpl();

	/**
	 * Persist Blog
	 *  @param blogDetails
	 */
 	public void createBlog(BlogDetails blogDetails) {
		 try {
			 if(blogDetails != null) {
				 blogDetailsData.put(blogDetails.getBlogId(), blogDetails);
			 } 
		 }catch(Exception e) {
			 throw new BlogCreationException("Error while creating blog entry");
		 }
	}

 	/**
 	 * Update Blog entry
 	 * @param blogDetails
 	 */
	public void updateBlog(BlogDetails blogDetails) {
		try {
			 if(blogDetails != null) {
				 blogDetailsData.put(blogDetails.getBlogId(), blogDetails);
			 } 
		 }catch(Exception e) {
			 System.out.println(e.getMessage());
		 }
	}
	
    /**
     * Get Blog by BlogId
     * 	As of now retrieving Blog from the Map 
     * 	@param blogId
     * 	@return blogDetails
     */
	public BlogDetails getBlog(String blogId) {
		BlogDetails blogDetails = null;
		try {
			blogDetails = blogDetailsData.get(blogId);
			if(blogDetails == null) {
				throw new BlogException();
			}
		}catch(BlogException e) {
			System.out.println(e.getMessage());
		}
		return blogDetails;
	}

	public Map<String,BlogDetails> getAllBlogs() {
		return blogDetailsData;
	}
}
