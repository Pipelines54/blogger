package com.test.blogger.caller;

import java.util.Scanner;

import com.test.blogger.constants.AppConstants;
import com.test.blogger.modal.BlogDetails;
import com.test.blogger.service.BlogService;
import com.test.blogger.serviceImpl.BlogServiceImpl;
import com.test.blogger.serviceImpl.SearchBlogServiceImpl;

public class BloggerIntializer {

	public static void main(String[] args) {

		BlogService blogService = new BlogServiceImpl();
		Scanner scanner = new Scanner(System.in);
		System.out.println(AppConstants.NUMBER_OF_BLOG_QUESTION);
		int numberOfBlogs = scanner.nextInt();

		for (int i = 0; i < numberOfBlogs; i++) {
			BlogDetails newBlogDetails = new BlogDetails();
			newBlogDetails.setBlogId(String.valueOf(i));
			blogService.createBlog(newBlogDetails);
		}
		
		String userId = AppConstants.USERID;
		System.out.println(AppConstants.BLOG_ID_QUESTION_TO_UPDATE);

		// As of now blogId is asking to the user
		String blogId = scanner.next();
		BlogDetails blogDetails = blogService.getBlog(blogId);
		if (blogDetails != null) {
			if (blogDetails.getUserId().equals(userId)) {
				blogService.updateBlog(blogDetails);
			} else {
				System.out.println(AppConstants.NOT_AUTHORIZED);
			}
		} else {
			System.out.println("Blog does not exits for blog Id: " + blogId);
		}

		
		System.out.println(AppConstants.BLOG_ARCHIVE_QUESTIONS);
		String archiveBlogId = scanner.next();
		BlogDetails blogDetailsArchive = blogService.archiveBlog(archiveBlogId);
		SearchBlogServiceImpl service = new SearchBlogServiceImpl();
		service.getAllUnPublishedBlog();

	}
}
