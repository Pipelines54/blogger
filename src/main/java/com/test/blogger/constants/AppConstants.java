package com.test.blogger.constants;

public interface AppConstants {
	
	public static final String USERID = "abc5012";
	
	public static final String BLOG_TITLE_QUESTIONS = "Please Enter Blog Title:";
	public static final String BLOG_CONTENT_QUESTIONS = "Please Enter Blog content:";
	public static final String BLOG_ARCHIVE_QUESTIONS = "Please enter blog Id to Archive::";
	public static final String BLOG_ID_QUESTION_TO_UPDATE = "Please Enter blog Id for blog update:";
	public static final String NUMBER_OF_BLOG_QUESTION = "How many blogs you wants to create ? ";
	
	
	//Email Config
	public static final String TO_EMAIL = "xyz@gmail.com";
	public static final String FROM_EMAIL = "mnc@gmail.com";
	public static final String SUBJECT_CREATE_EMAIL = "Your mail is Created";
	public static final String SUBJECT_UPDATE_EMAIL = "Your mail is Updated";
	public static final String SUBJECT_ARCHIVE_EMAIL = "Your mail is Archive";
	public static final String SUBJECT_APPROVE_EMAIL = "Your mail is Approved";
	public static final String SUBJECT_REJECT_EMAIL = "Your mail is rejected";
	
	//Security Messages
	public static final String NOT_AUTHORIZED = "Not Authorized";

}
